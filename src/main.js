import { createApp, h } from "vue";
import { createApolloProvider } from "@vue/apollo-option";
import { ApolloClient, InMemoryCache } from "@apollo/client/core";
import App from "./App.vue";

const defaultClient = new ApolloClient({
  uri: "https://rickandmortyapi.com/graphql",
  cache: new InMemoryCache(),
});

const apolloProvider = createApolloProvider({
  defaultClient,
});

const app = createApp({
  render: () => h(App),
});

app.use(apolloProvider);

app.mount("#app");
